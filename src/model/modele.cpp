
#include "modele.h"

void Modele::assignerVue(Vue* v) {
    this->vue = v;
}

void Modele::obtientDonneesMasses() {
    this->vue->afficherMasses("masses");
}

void Modele::obtientDonneesDistances() {
    this->vue->afficherDistances("distances");
}

void Modele::obtientDonneesDiametres() {
    this->vue->afficherDiametres("diametres");
}


