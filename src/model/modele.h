#ifndef __MODELE__
#define __MODELE__

#include "../view/vue.h"

// Forward declaration to resolve the 
// circular dependency required for MVC.
class Vue;

class Modele {
    private:
        Vue* vue;
    public:
        void assignerVue(Vue* v);
        void obtientDonneesMasses();
        void obtientDonneesDistances();
        void obtientDonneesDiametres();
};

#endif
