
#include "model/modele.h"
#include "view/vue.h"
#include "controller/controleur.h"

int main(int argc, char** argv) {

    // Création du modèle, de la vue et du contrôleur.
    Modele* le_modele = new Modele();
    Vue* la_vue = new Vue();
    Controleur* le_controleur = new Controleur();

    // Assignation des relations MVC.
    la_vue->assignerControleur(le_controleur);
    le_controleur->assignerModele(le_modele);
    le_modele->assignerVue(la_vue);

    // Lancement de l'app en affichant la vue initiale.
    la_vue->afficherMenuPrincipal();

    // Nettoyage des objets créés.
    delete le_modele;
    delete la_vue;
    delete le_controleur;

    return 0;
}
