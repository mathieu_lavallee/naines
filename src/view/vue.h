#ifndef __VUE__
#define __VUE__

#include <iostream> // Pour std::cout
#include <string>

#include "../controller/controleur.h"

// Forward declaration to resolve the 
// circular dependency required for MVC.
class Controleur;

class Vue {
    private:
        Controleur* controleur;
    public:
        void assignerControleur(Controleur* c);
        void afficherMenuPrincipal();
        void afficherMasses(std::string data);
        void afficherDistances(std::string data);
        void afficherDiametres(std::string data);
};

#endif
