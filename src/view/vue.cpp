
#include "vue.h"

void Vue::assignerControleur(Controleur* c) {
    this->controleur = c;
}

void Vue::afficherMenuPrincipal() {
    std::cout << std::endl;
    std::cout << " === MENU PRINCIPAL === " << std::endl;
    std::cout << " 1. Afficher les masses" << std::endl;
    std::cout << " 2. Afficher les distances" << std::endl;
    std::cout << " 3. Afficher les diamètres" << std::endl;
    std::cout << std::endl;
    std::cout << " Entrez votre choix : ";

    // Passe la main au contrôleur qui va gérer la réponse.
    this->controleur->repondreMenuPrincipal();
}

void Vue::afficherMasses(std::string data) {
    std::cout << data << std::endl;
}

void Vue::afficherDistances(std::string data) {
    std::cout << data << std::endl;
}

void Vue::afficherDiametres(std::string data) {
    std::cout << data << std::endl;
}


