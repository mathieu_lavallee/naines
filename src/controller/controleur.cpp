
#include "controleur.h"

void Controleur::assignerModele(Modele* m) {
    this->modele = m;
}

void Controleur::repondreMenuPrincipal() {
    int reponse;

    std::cin >> reponse;

    // Passe la main au modèle selon la réponse donnée.
    switch (reponse) {
        case 1: this->modele->obtientDonneesMasses(); break;
        case 2: this->modele->obtientDonneesDistances(); break;
        case 3: this->modele->obtientDonneesDiametres(); break;
    };
}


