#ifndef __CONTROLEUR__
#define __CONTROLEUR__

#include <iostream> // Pour std::cin

#include "../model/modele.h"

// Forward declaration to resolve the 
// circular dependency required for MVC.
class Modele;

class Controleur {
    private:
        Modele* modele;
    public:
        void assignerModele(Modele* m);
        void repondreMenuPrincipal();
};

#endif
