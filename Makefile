
EXEC=naines
BINAIRE=bin
SOURCE=src

# ===================
# Commande par défaut
# ===================

all: $(BINAIRE)/$(EXEC)
	./$(BINAIRE)/$(EXEC)

# ===========================
# Compilation de l'exécutable
# ===========================

$(BINAIRE)/$(EXEC): $(BINAIRE)/main.o $(BINAIRE)/vue.o $(BINAIRE)/modele.o $(BINAIRE)/controleur.o
	g++ -o $@ $^

$(BINAIRE)/main.o: $(SOURCE)/main.cpp $(SOURCE)/model/modele.h $(SOURCE)/view/vue.h $(SOURCE)/controller/controleur.h
	mkdir -p $(BINAIRE)
	g++ -o $@ -c $< 

$(BINAIRE)/vue.o: $(SOURCE)/view/vue.cpp $(SOURCE)/view/vue.h $(SOURCE)/controller/controleur.h
	g++ -o $@ -c $< 

$(BINAIRE)/modele.o: $(SOURCE)/model/modele.cpp $(SOURCE)/model/modele.h $(SOURCE)/view/vue.h
	g++ -o $@ -c $< 

$(BINAIRE)/controleur.o: $(SOURCE)/controller/controleur.cpp $(SOURCE)/controller/controleur.h $(SOURCE)/model/modele.h
	g++ -o $@ -c $< 

# ==========================
# Compilation pour les tests
# ==========================


# ===========
# Utilitaires
# ===========

clean:
	rm -rf $(BINAIRE)/*.o
	rm -rf $(BINAIRE)/$(EXEC)


